export default {
    cloneObject(o)
    {
        let out, v, key;
        out = Array.isArray(o) ? [] : {};
        for (key in o) {
            v = o[key];
            out[key] = (typeof v === "object" && v !== null) ? this.cloneObject(v) : v;
        }
        return out;
    }
}