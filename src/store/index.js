import Vue from 'vue'
import Vuex from 'vuex'

import CreateSchema from './modules/Schema/create.js'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        CreateSchema
    },
    getters: {

    },
    mutations: {

    },
    actions: {

    },
});