const module = {
    namespaced: true,
    state: {
        rows: [],
        index: 0,
        templateRow: {
            table: '',
            func: '',
            column: '',
            index: '',
        },
    },
    mutations: {
        addEmptyRow(state)
        {
            //clone the array because next actions would also change the template
            let row = this._vm.$Helpers.cloneObject(state.templateRow);
            row.index = state.index;
            state.index++;
            state.rows.push(row);
        },
        removeRow(state, payload)
        {
            console.log(payload.index);
            state.rows = state.rows.filter(function (row, index) {
                if (row.index !== payload.index)
                {
                    return true;
                }
            });
        }
    },
    getters: {
        getRows (state, getters, rootState) {
            return state.rows;
        }
    },
    actions: {
        addEmptyRow({state, commit, rootState})
        {
            commit('addEmptyRow');
        },
        removeRow({state, commit, rootState}, payload)
        {
            commit('removeRow', payload);
        },
    },
};

export default module;